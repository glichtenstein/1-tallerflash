# Taller Flash N° 1
# Ejercicio 2 - Problema de Orion
# Autor: Gabriel Lichtenstein
# Fecha: 30/03/2016
# Utiliza Python 3

# referencia: 
	# orion de 1 es 3
	# orion de 2 da 7
	# orion de 3 31

# Función Auxiliar esPrimo:
	# devuelve True si x es un número primo
def esPrimo(x): 
	cont = 0
	if x <=1: # paso de control
		return False		
	for i in range(2, x):
		if (x % i == 0): # si alguno tiene resto distinto de cero no es primo
			cont+=1
	if cont != 0:
		return False
	else:
		return True

#~ # Prueba de primos:
#~ print(esPrimo(15))
#~ print(esPrimo(2))
#~ print(esPrimo(1))
#~ print(esPrimo(7))
#~ print(esPrimo(5))

# Funcion principal Orion
	# Devuelve el enecimo numero que cumple con esPrimo y 2**k-1
def Orion(n):
	
	k = 1 # k cuenta el exponente de 2**k-1
	contador = 0 # contador suma cuantos me encontre que cumplen con las dos condiciones.
	res = 0 # res me da el enecimo numero que cumple con ambas condiciones.
	
	while (contador < n):		
		res = (2**k - 1)
		if esPrimo(res):
			contador+=1
		
		k += 1 
		
	return res
		
# Prueba Orion
print (Orion(1))
print (Orion(2))
print (Orion(3))

