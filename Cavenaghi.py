
# Taller Flash N° 1
# Ejercicio 1 - Problema de Cavenaghi
# Autor: Gabriel Lichtenstein
# Fecha: 30/03/2016

# usando for
def Cavenaghi(n):
# Función que calcula la sumatoria de la ecuación del Problema 
# de Cavenaghi desde 1 a n inclusive:
	sum = 0	
	for i in range(1, n+1):
		sum = ((-1)**(i+1) * 2) / ((2 * i) -1) + sum		
	return sum
			
res = Cavenaghi(1000)
print (res)

#~ # usando while
#~ def Cavenaghi(n):
	#~ i = 1
	#~ sum = 0
	#~ while (i <= n):		
		#~ sum = ((-1)**(i+1) * 2) / ((2 * i) -1 ) + sum
		#~ i = i + 1
	#~ return sum
		
#~ res1 = Cavenaghi(1000)
#~ print (res1)
